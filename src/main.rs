#[macro_use]
extern crate bincode;
extern crate log;
extern crate log4rs;
extern crate serde;
extern crate serde_derive;

use log::{info, trace, warn};
use std::net::{SocketAddr, ToSocketAddrs, UdpSocket};
use std::sync::Arc;

mod guessing_game;
mod messages;

use self::messages::*; //{Message, GameBuilder};

// Listener thread, responds to listen for messages
// Executor Threads
// Mutex to protect socket
// Potential urgent tasks, Quiq, Heartbeat, Quit

// Listener => Message
// Message => Executor (Can Lock Mutex)

fn main() -> std::io::Result<()> {
    {
        log4rs::init_file("config/log4rs.yaml", Default::default()).unwrap();
        info!("Booting up");

        let listener_state =
            guessing_game::ListenerState::buildListenerState("35.163.138.220:12001".to_string())
                .unwrap();
        let ui_handle = guessing_game::ui::run(Arc::clone(&listener_state));
        let exec_handle = guessing_game::executor::run(Arc::clone(&listener_state));
        let server_handle = guessing_game::listener::listen(Arc::clone(&listener_state));
        let handles = vec![ui_handle, exec_handle, server_handle];
        for handle in handles {
            handle.join();
        }
    }
    Ok(())
}
