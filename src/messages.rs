extern crate byteorder;
use byteorder::{BigEndian, ByteOrder};
use log::{info, trace, warn};
use std::collections::VecDeque;

#[derive(Debug, Clone)]
pub enum Message {
    StartGame {
        message_type: u16,
        a_number: String,
        last_name: String,
        first_name: String,
        public_alias: String,
    },
    GameDef {
        message_type: u16,
        game_id: u16,
        hint: String,
        definition: String,
    },
    Guess {
        message_type: u16,
        game_id: u16,
        guess: String,
    },
    Answer {
        message_type: u16,
        game_id: u16,
        result: u8,
        score: u16,
        hint: String,
    },
    GetHint {
        message_type: u16,
        game_id: u16,
    },
    Hint {
        message_type: u16,
        game_id: u16,
        hint: String,
    },
    Heartbeat {
        message_type: u16,
        game_id: u16,
    },
    Ack {
        message_type: u16,
        game_id: u16,
    },
    Error {
        message_type: u16,
        game_id: u16,
        error_text: String,
    },
    Exit {
        message_type: u16,
        game_id: u16,
    },
}

pub mod GameBuilder {
    use super::*;

    fn serialize_u16(val: u16) -> Option<Vec<u8>> {
        bincode::config().big_endian().serialize(&val).ok()
    }

    fn serialize_string(str_val: &str) -> Option<Vec<u8>> {
        let size: u16 = str_val.len() as u16 * 2;
        let size_header = bincode::config().big_endian().serialize(&size);

        let mut all = vec![];
        all.append(&mut size_header.unwrap());

        for current in str::encode_utf16(&str_val) {
            let slice = bincode::config().big_endian().serialize(&current);
            all.append(&mut slice.unwrap());
        }

        Some(all)
    }

    fn serialize_strings(strings: Vec<&str>) -> Option<Vec<u8>> {
        let mut all = vec![];

        for string in strings {
            match serialize_string(string) {
                Some(mut serialized) => all.append(&mut serialized),
                None => return None,
            };
        }
        Some(all)
    }

    fn message_id(message: &Message) -> Option<u16> {
        match message {
            Message::StartGame { .. } => Some(1),
            Message::Guess { .. } => Some(3),
            Message::GetHint { .. } => Some(5),
            Message::Exit { .. } => Some(7),
            Message::Ack { .. } => Some(8),
            _ => None,
        }
    }

    fn string_fields(message: &Message) -> Vec<&str> {
        match message {
            Message::StartGame {
                message_type,
                a_number,
                last_name,
                first_name,
                public_alias,
            } => vec![a_number, last_name, first_name, public_alias],
            Message::Guess {
                message_type,
                game_id,
                guess,
            } => vec![guess],
            _ => vec![],
        }
    }

    pub fn serialize(message: Message) -> Option<Vec<u8>> {
        let mut all = vec![];
        let id = message_id(&message);
        let serialized_id = serialize_u16(id?);

        all.append(&mut serialized_id?);

        let mut additional_bytes = match message {
            Message::StartGame { .. } => vec![],
            Message::Guess { game_id, .. } => serialize_u16(game_id)?,
            Message::GetHint { game_id, .. } => serialize_u16(game_id)?,
            Message::Ack { game_id, .. } => serialize_u16(game_id)?,
            Message::Exit { game_id, .. } => serialize_u16(game_id)?,
            _ => vec![],
        };

        all.append(&mut additional_bytes);

        match serialize_strings(string_fields(&message)) {
            Some(mut serialized) => all.append(&mut serialized),
            None => return None,
        }

        Some(all)
    }

    fn deserialize_string(bytes: &[u8]) -> (usize, Option<String>) {
        let len = BigEndian::read_u16(&bytes) as usize;
        let b = &bytes[2..len + 2];

        let utf16_buf: Vec<u16> = b.chunks(2).map(|c| BigEndian::read_u16(&c)).collect();

        match String::from_utf16(&utf16_buf).ok() {
            Some(val) => (len + 2, Some(val)),
            None => (0 as usize, None),
        }
    }

    pub fn deserialize_start_game(copy: &[u8]) -> Option<Message> {
        let mut offset = 2;
        let mut results = VecDeque::new();

        for _ in 0..4 {
            let (read_len, result) = deserialize_string(&copy[offset..]);
            offset += read_len;
            results.push_back(result);
        }

        Some(Message::StartGame {
            message_type: 1,
            a_number: results.pop_front()??,
            last_name: results.pop_front()??,
            first_name: results.pop_front()??,
            public_alias: results.pop_front()??,
        })
    }

    pub fn deserialize_game_def(bytes: &[u8]) -> Option<Message> {
        let game_id = BigEndian::read_u16(&bytes[2..]);

        let mut offset = 4;
        let mut results = VecDeque::new();

        for _ in 0..2 {
            let (read_len, result) = deserialize_string(&bytes[offset..]);
            offset += read_len;
            results.push_back(result);
        }

        Some(Message::GameDef {
            message_type: 2,
            game_id,
            hint: results.pop_front()??,
            definition: results.pop_front()??,
        })
    }

    pub fn deserialize_heartbeat(bytes: &[u8]) -> Option<Message> {
        let game_id = BigEndian::read_u16(&bytes[2..]);

        Some(Message::Heartbeat {
            message_type: 10,
            game_id,
        })
    }

    pub fn deserialize_answer(bytes: &[u8]) -> Option<Message> {
        let game_id = BigEndian::read_u16(&bytes[2..]);
        let result = bytes[4];
        let score = BigEndian::read_u16(&bytes[5..]);
        let (read_len, hint) = deserialize_string(&bytes[7..]);

        Some(Message::Answer {
            message_type: 4,
            game_id,
            result,
            score,
            hint: hint.unwrap_or("".to_string()),
        })
    }

    pub fn deserialize_error(bytes: &[u8]) -> Option<Message> {
        let game_id = BigEndian::read_u16(&bytes[2..]);
        let (read_len, error_text) = deserialize_string(&bytes[4..]);

        Some(Message::Error {
            message_type: 9,
            game_id,
            error_text: error_text.unwrap_or("".to_string()),
        })
    }

    pub fn deserialize_hint(bytes: &[u8]) -> Option<Message> {
        let game_id = BigEndian::read_u16(&bytes[2..]);
        let (read_len, hint) = deserialize_string(&bytes[4..]);

        Some(Message::Hint {
            message_type: 6,
            game_id,
            hint: hint.unwrap_or("".to_string()),
        })
    }

    pub fn deserialize_ack(bytes: &[u8]) -> Option<Message> {
        let game_id = BigEndian::read_u16(&bytes[2..]);

        Some(Message::Ack {
            message_type: 8,
            game_id,
        })
    }

    pub fn deserialize(bytes: Vec<u8>) -> Option<Message> {
        let id = BigEndian::read_u16(&bytes);

        let m = match id {
            1 => deserialize_start_game(&bytes),
            2 => deserialize_game_def(&bytes),
            4 => deserialize_answer(&bytes),
            6 => deserialize_hint(&bytes),
            8 => deserialize_ack(&bytes),
            9 => deserialize_error(&bytes),
            10 => deserialize_heartbeat(&bytes),
            _ => {
                trace!("Received unrecognized message id: {:?}", id);
                None
            }
        };

        trace!("Received Message: {:?}", &m);
        m
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_serializes() {
        let expected_bytes = vec![
            0, 1, 0, 18, 0, 0x41, 0, 48, 0, 49, 0, 57, 0, 54, 0, 49, 0, 50, 0, 51, 0, 48, 0, 12, 0,
            66, 0, 101, 0, 110, 0, 111, 0, 105, 0, 116, 0, 8, 0, 78, 0, 105, 0, 99, 0, 107, 0, 12,
            0, 78, 0, 105, 0, 99, 0, 107, 0, 121, 0, 66,
        ];

        let message = Message::StartGame {
            message_type: 1,
            a_number: "A01961230".to_string(),
            last_name: "Benoit".to_string(),
            first_name: "Nick".to_string(),
            public_alias: "NickyB".to_string(),
        };

        // Should be big endian, strings <lenght in total bytes><unicode string>
        // Or just to a buffer

        assert_eq!(GameBuilder::serialize(message), Some(expected_bytes));
    }

    #[test]
    fn it_deserializes() {
        let bytes = vec![
            0, 1, 0, 18, 0, 0x41, 0, 48, 0, 49, 0, 57, 0, 54, 0, 49, 0, 50, 0, 51, 0, 48, 0, 12, 0,
            66, 0, 101, 0, 110, 0, 111, 0, 105, 0, 116, 0, 8, 0, 78, 0, 105, 0, 99, 0, 107, 0, 12,
            0, 78, 0, 105, 0, 99, 0, 107, 0, 121, 0, 66,
        ];

        let expected_message = Message::StartGame {
            message_type: 1,
            a_number: "A01961230".to_string(),
            last_name: "Benoit".to_string(),
            first_name: "Nick".to_string(),
            public_alias: "NickyB".to_string(),
        };

        let result = GameBuilder::deserialize(bytes);
        match result {
            Some(Message::StartGame {
                message_type,
                a_number,
                last_name,
                first_name,
                public_alias,
            }) => {
                if let Message::StartGame {
                    message_type: expected_type,
                    a_number: expected_a_number,
                    last_name: expected_last_name,
                    first_name: expected_first_name,
                    public_alias: expected_public_alias,
                } = expected_message
                {
                    assert_eq!(message_type, expected_type);
                    assert_eq!(a_number, expected_a_number);
                    assert_eq!(last_name, expected_last_name);
                    assert_eq!(first_name, expected_first_name);
                    assert_eq!(public_alias, expected_public_alias);
                } else {
                    assert!(false);
                }
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }
}
