use crate::messages::{GameBuilder, Message};
use log::{info, trace, warn};
use std::collections::VecDeque;
use std::io;
use std::io::{stdin, stdout, Write};
use std::io::{Error, ErrorKind};
use std::net::{SocketAddr, ToSocketAddrs, UdpSocket};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time;
use std::time::Duration;

#[derive(Debug, Clone)]
enum Job {
    Guess(String),
    Message(Message),
    UpdateGameState(GameState),
    StartGame {
        a_num: String,
        last_name: String,
        first_name: String,
        public_alias: String,
    },
    Hint,
    Exit,
}

#[derive(Debug, Clone)]
pub struct GameState {
    game_id: Option<u16>,
    player_messages: VecDeque<String>,
    hint: Option<String>,
    definition: Option<String>,
    terminate: bool,
}

#[derive(Debug)]
pub struct ListenerState {
    socket: Mutex<UdpSocket>,
    queue: Mutex<VecDeque<Job>>,
    game_state: Mutex<GameState>,
}

impl ListenerState {
    pub fn buildListenerState(address: String) -> Result<Arc<ListenerState>, std::io::Error> {
        let half_second = Duration::new(0, 500000000);
        let socket = UdpSocket::bind("127.0.0.1:34254")?;
        socket.set_read_timeout(Some(half_second))?;
        socket
            .connect("127.0.0.1:12001")
            .expect("connect function failed");

        Ok(Arc::new(ListenerState {
            socket: Mutex::new(socket),
            queue: Mutex::new(VecDeque::new()),
            game_state: Mutex::new(GameState {
                game_id: None,
                player_messages: VecDeque::new(),
                definition: None,
                hint: None,
                terminate: false,
            }),
        }))
    }

    // Network - Used by executor
    pub fn sendMessage(&self, message: Message) -> Result<usize, Error> {
        {
            trace!("Sending message: {:?}", &message);
            let cp = message.clone();
            let s = self.socket.lock().unwrap();
            match GameBuilder::serialize(message) {
                Some(bytes) => {
                    let result = s.send(&bytes);
                    match &result {
                        Ok(size) => trace!("Sent: {} bytes, {:?}", size, &bytes),
                        Err(e) => warn!("{:?}", e),
                    }
                    result
                }
                None => {
                    trace!("Faild to serialize message: {:?}", &cp);
                    Err(Error::new(ErrorKind::Other, "Failed to serialize message"))
                }
            }
        }
    }

    fn receiveMessage(&self) -> Option<Message> {
        let s = self.socket.lock().unwrap();
        let mut buf = [0; 500];
        //trace!("Entering Recv");
        match s.recv(&mut buf) {
            Ok(received) => {
                //println!("Received bytes: {:?}", &buf[..received]);
                GameBuilder::deserialize(buf[..received].to_vec())
            }
            Err(e) => {
                /*trace!("Leaving Recv");*/
                None
            }
        }
    }

    // Job -> Called by listener and UI to add jobs to the queue
    fn recieveAppendMessage(&self) {
        match self.receiveMessage() {
            Some(message) => {
                let mut q = self.queue.lock().unwrap();
                let j = Job::Message(message);
                trace!("Added job to queue: {:?}", &j);
                q.push_back(j);
            }
            _ => {}
        }
    }

    fn start_game(
        &self,
        a_num: String,
        last_name: String,
        first_name: String,
        public_alias: String,
    ) {
        let j = Job::StartGame {
            a_num,
            last_name,
            first_name,
            public_alias,
        };
        self.queue.lock().unwrap().push_back(j);
    }

    fn force_stop(&self) {
        let game_state = self.get_game_state();
        self.update_game_state(GameState {
            terminate: true,
            ..game_state
        });
    }
    fn stop_game(&self) {
        self.queue.lock().unwrap().push_back(Job::Exit);
    }

    fn get_hint(&self) {
        self.queue.lock().unwrap().push_back(Job::Hint);
    }

    fn guess(&self, val: String) {
        self.queue.lock().unwrap().push_back(Job::Guess(val));
    }

    // Accessor -> Used mostly by executor to update state
    fn next_job(&self) -> Option<Job> {
        let j = self.queue.lock().unwrap().pop_front();
        match &j {
            Some(job) => trace!("Removing job from queue: {:?}", job),
            _ => {}
        }
        j
    }

    fn update_game_state(&self, updated_state: GameState) {
        trace!("Updated game state to: {:?}", &updated_state);
        *self.game_state.lock().unwrap() = updated_state;
    }

    fn get_game_state(&self) -> GameState {
        self.game_state.lock().unwrap().clone()
    }
}

pub mod listener {
    use super::*;
    pub fn listen(state: Arc<ListenerState>) -> std::thread::JoinHandle<()> {
        thread::spawn(move || loop {
            let game_state = state.get_game_state();

            if (game_state.terminate) {
                trace!("Stopping server thread");
                return;
            }
            let ten_millis = time::Duration::from_millis(100);
            thread::sleep(ten_millis);
            Arc::clone(&state).recieveAppendMessage()
        })
    }
}

pub mod executor {
    use super::*;

    fn handle_message_job(state: &Arc<ListenerState>, job: Job) {
        if let Job::Message(m) = job {
            match &m {
                Message::GameDef { .. } => handle_game_def(state, m),
                Message::Heartbeat { .. } => handle_heartbeat(state, m),
                Message::Answer { .. } => handle_answer(state, m),
                Message::Hint { .. } => handle_hint(state, m),
                Message::Ack { .. } => handle_ack(state, m),
                _ => {
                    trace!("Unsupported message job!");
                }
            }
        } else {
            trace!("Not a message job");
        }
    }

    fn handle_ack(state: &Arc<ListenerState>, message: Message) {
        if let Message::Ack { game_id, .. } = message {
            let game_state = state.get_game_state();
            state.update_game_state(GameState {
                terminate: true,
                ..game_state
            });
        } else {
            warn!("Cannot process game def message");
        }
    }

    fn handle_hint(state: &Arc<ListenerState>, message: Message) {
        if let Message::Hint { game_id, hint, .. } = message {
            let game_state = state.get_game_state();
            state.update_game_state(GameState {
                hint: Some(hint),
                ..game_state
            });
        } else {
            warn!("Cannot process game def message");
        }
    }

    fn handle_answer(state: &Arc<ListenerState>, message: Message) {
        if let Message::Answer {
            game_id,
            result,
            score,
            hint,
            ..
        } = message
        {
            let game_state = state.get_game_state();

            let player_messages = if (result > 0) {
                let m = format!("You won! Your score is: {}", score);
                let mut v = VecDeque::new();
                v.push_back(m);
                v
            } else {
                VecDeque::new()
            };

            state.update_game_state(GameState {
                game_id: Some(game_id),
                hint: Some(hint),
                terminate: result > 0,
                player_messages,
                ..game_state
            });
        } else {
            warn!("Cannot process game def message");
        }
    }

    fn handle_heartbeat(state: &Arc<ListenerState>, message: Message) {
        if let Message::Heartbeat { game_id, .. } = message {
            let response = Message::Ack {
                message_type: 8,
                game_id,
            };
            trace!("Responded to heartbeat: {:?}", &response);
            state.sendMessage(response);
        } else {
            warn!("Cannot process game def message");
        }
    }

    fn handle_game_def(state: &Arc<ListenerState>, message: Message) {
        if let Message::GameDef {
            game_id,
            hint,
            definition,
            ..
        } = message
        {
            let game_state = state.get_game_state();

            state.update_game_state(GameState {
                game_id: Some(game_id),
                player_messages: VecDeque::new(),
                hint: Some(hint),
                definition: Some(definition),
                ..game_state
            });
        } else {
            warn!("Cannot process game def message");
        }
    }

    fn handle_start_game_job(state: &Arc<ListenerState>, job: Job) {
        match job {
            Job::StartGame {
                a_num,
                last_name,
                first_name,
                public_alias,
            } => {
                state.sendMessage(Message::StartGame {
                    message_type: 1,
                    a_number: a_num,
                    last_name,
                    first_name,
                    public_alias,
                });
            }
            _ => warn!("Cannot handle job!"),
        }
    }

    fn handle_guess_job(state: &Arc<ListenerState>, guess: String) {
        let current_state = state.get_game_state();
        let m = Message::Guess {
            message_type: 3,
            game_id: current_state.game_id.unwrap_or(0),
            guess: guess.trim().to_string(),
        };
        state.sendMessage(m);
    }

    fn handle_hint_job(state: &Arc<ListenerState>) {
        let current_state = state.get_game_state();
        let m = Message::GetHint {
            message_type: 5,
            game_id: current_state.game_id.unwrap_or(0),
        };
        state.sendMessage(m);
    }

    fn handle_exit(state: &Arc<ListenerState>, job: Job) {
        let current_state = state.get_game_state();
        let m = Message::Exit {
            message_type: 7,
            game_id: current_state.game_id.unwrap_or(0),
        };
        state.sendMessage(m);
    }

    fn handleJob(state: &Arc<ListenerState>, job: Job) {
        let cp = job.clone();
        trace!("Processing Job: {:?}", &cp);
        match &job {
            Job::StartGame { .. } => handle_start_game_job(state, job),
            Job::Message(m) => handle_message_job(state, job),
            Job::Guess(word) => handle_guess_job(state, word.to_string()),
            Job::Hint => handle_hint_job(state),
            Job::Exit => handle_exit(state, job),
            _ => {
                warn!("Unsupported Job!");
            }
        }

        trace!("Done Processing Job: {:?}", &cp);
    }

    pub fn run(state: Arc<ListenerState>) -> thread::JoinHandle<()> {
        let s = Arc::clone(&state);
        let ten_millis = time::Duration::from_millis(10);

        thread::spawn(move || {
            loop {
                let game_state = state.get_game_state();
                if (game_state.terminate) {
                    trace!("Stopping executor thread");
                    return;
                }

                match s.next_job() {
                    Some(j) => handleJob(&s, j),
                    _ => {
                        thread::sleep(ten_millis);
                    }
                }
            }
        })
    }
}

pub mod ui {

    use super::*;
    pub fn run(state: Arc<ListenerState>) -> thread::JoinHandle<()> {
        let s = Arc::clone(&state);
        let ten_millis = time::Duration::from_millis(10);

        thread::spawn(move || {
            trace!("Starting UI Thread");
            loop {
                let game_state = state.get_game_state();

                if (game_state.terminate) {
                    trace!("Stopping UI thread");
                    for message in game_state.player_messages {
                        println!("{}", message)
                    }

                    return;
                }
                let millis = time::Duration::from_millis(1000);
                thread::sleep(millis);
                start_game_prompt(&s);
            }
        })
    }

    fn ask_start_game(state: &Arc<ListenerState>) {
        println!("Would you like to play a game? y) n)");
        match get_input() {
            Some(ref val) if val == "y\n" => {
                trace!("Starting Game!");

                println!("What is your A#?");
                let a_num = get_input().unwrap_or("#0000000".to_string());

                println!("What is your Last Name?");
                let last_name = get_input().unwrap_or("".to_string());

                println!("What is your First Name?");
                let first_name = get_input().unwrap_or("".to_string());

                println!("What is your public alias?");
                let alias = get_input().unwrap_or(first_name.clone());

                state.start_game(a_num, last_name, first_name, alias);
            }
            _ => {
                println!("Have a nice day!");
                trace!("Stopping Game!");
                state.force_stop();
            }
        }
    }

    fn print_word_status(state: &Arc<ListenerState>) {
        let game_state = state.get_game_state();
        println!(
            "Description: {} \n Word: {}",
            game_state.definition.unwrap_or("-".to_string()),
            game_state.hint.unwrap_or("-".to_string())
        );
    }

    fn ask_action(state: &Arc<ListenerState>) {
        print_word_status(state);
        println!("Would you like to: \n Enter any text to guess \n :h) get a hint \n :q) quit");
        match get_input() {
            Some(ref val) if val == ":h\n" => {
                println!("Getting a hint!");
                state.get_hint();
            }
            Some(ref val) if val == ":q\n" => {
                println!("Have a nice day!");
                trace!("Stopping Game!");
                state.stop_game();
            }
            Some(val) => state.guess(val),
            _ => {
                println!("Have a nice day!");
                trace!("Stopping Game!");
                state.force_stop();
            }
        }
    }

    fn start_game_prompt(state: &Arc<ListenerState>) {
        let game_id = { state.game_state.lock().unwrap().game_id };
        match game_id {
            Some(id) => ask_action(state),
            None => ask_start_game(state),
        }
    }

    fn get_input() -> Option<String> {
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_) => Some(input),
            Err(error) => {
                warn!("error: {}", error);
                None
            }
        }
    }
}
